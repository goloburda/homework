<?php

    $a = $_POST["a"];
    $b = $_POST["b"];
    $c = $_POST["c"];

    echo '<br>';
    echo $a . ' ' . $b . ' ' . $c . '<br>';

// Max number
    if($a >= $b && $a >= $c) {
        echo 'Max: ' . $a;
    } elseif($a <= $b && $b >= $c) {
        echo 'Max: ' . $b;
    } else {
        echo 'Max: ' . $c;
    }

    echo '<br>';

// Min number
    if($a <= $b && $a <= $c) {
        echo 'Min: ' . $a;
    } elseif($a >= $b && $b <= $c) {
        echo 'Min: ' . $b;
    } else {
        echo 'Min: ' . $c;
    }

    echo '<br>';

// Equal zero
    $tmpZero = 0;
    if($a == 0) {
        $tmpZero++;
        echo 'A = 0' . '<br>';
    } 
    if($b == 0) {
        $tmpZero++;
        echo 'B = 0' . '<br>';
    } 
    if($c == 0) {
        $tmpZero++;
        echo 'C = 0' . '<br>';
    }
    echo '<br>' . 'Numbers equal zero: ' . $tmpZero;

    echo '<br>';
// Greater A
    if($a < $b && $a < $c) {
        echo 'Numbers greater than A: 2';
    } 
    elseif($a < $b || $a < $c) {
        echo 'Numbers greater than A: 1';
    } else {
        echo 'Number A is the greatest';
    }

// SUM
 echo '<h2>Sum is: ' . ($a + $b + $c) . '</h2>';
 echo '<h2>Average is: ' . (($a + $b + $c) / 3) . '</h2>';

// %2
    if( ($a % 2 == 0) && ($b % 2 == 0) && ($c % 2 == 0) ) {
        echo 'Three numbers divided by 2'; 
    } elseif ( ( ($a % 2 == 0) && ($b % 2 == 0) )  || ( ($a % 2 == 0) && ($c % 2 == 0) ) || ( ($b % 2 == 0) && ($c % 2 == 0)  )) {
        echo 'Two numbers divided by 2'; 
    } elseif ( ($a % 2 == 0) || ($b % 2 == 0) || ($c % 2 == 0) ) {
        echo 'One number divided by 2'; 
    } else {
        echo 'No one number divided by 2'; 
    }

    echo '<br>';

// %3 
    if( ($a % 3 == 0) && ($b % 3 == 0) && ($c % 3 == 0) ) {
        echo 'Three numbers divided by 3'; 
    } elseif ( ( ($a % 3 == 0) && ($b % 3 == 0) )  || ( ($a % 3 == 0) && ($c % 3 == 0) ) || ( ($b % 3 == 0) && ($c % 3 == 0)  )) {
        echo 'Two numbers divided by 3'; 
    } elseif ( ($a % 3 == 0) || ($b % 3 == 0) || ($c % 3 == 0) ) {
        echo 'One number divided by 3'; 
    } else {
        echo 'No one number divided by 3';
        echo '<br>';
        echo 'No one number divided by 2 and 3';  
    }

    echo '<br>';

//% 2 and 3
    if( ($a % 2 == 0) && ($b % 2 == 0) && ($c % 2 == 0) ) {
        if( ($a % 3 == 0) && ($b % 3 == 0) && ($c % 3 == 0) ){
            echo 'Three numbers divided by 2 and 3'; 
        } 
    }
        
    elseif  ( ( ($a % 2 == 0) && ($b % 2 == 0) )  || ( ($a % 2 == 0) && ($c % 2 == 0) ) || ( ($b % 2 == 0) && ($c % 2 == 0)  )) {
        if( ( ( ($a % 3 == 0) && ($b % 3 == 0) )  || ( ($a % 3 == 0) && ($c % 3 == 0) ) || ( ($b % 3 == 0) && ($c % 3 == 0)  )) ) {
            echo 'Two numbers divided by 2 and 3'; 
        }
    }
    
    elseif ( ($a % 2 == 0) || ($b % 2 == 0) || ($c % 2 == 0) ) {
        if ( ($a % 3 == 0) || ($b % 3 == 0) || ($c % 3 == 0) ){
            echo 'One number divided by 2 and 3'; 
        }
    } 

