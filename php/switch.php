<?php

$day = date("j");


switch($day){
    case ($day < 11):
        echo 'Firs decade';
        break;
    case ($day < 21):
        echo 'Second decade';
        break;
    case ($day >= 21):
        echo 'Third decade';
        break;
}

echo '<br>';

$browser = 'Chrome';

switch($browser) {
    case 'IE':
        echo 'Sorry, you have got IE ';
        break;
    case 'Chrome':
    case 'Firefox':
    case 'Safari':
    case 'Opera':
        echo 'You are welcome: ' . $browser;
        break;
    default:
        echo 'Hope ' . $browser;
        break;
}