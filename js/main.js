alert(1);

var checkBtn = document.querySelector('#check');
var switchBtn = document.querySelector('#switch');
var clearBtn = document.querySelector('#clear');
var arrayBtn = document.querySelector('#array');
var input = document.querySelectorAll(' .taskInput ');
var p = document.querySelector("#pp");
var h3 = document.querySelector("#hh");

var arr = [];


checkBtn.addEventListener('click', checkSet);
switchBtn.addEventListener('click', switchDoc);
arrayBtn.addEventListener('click', showArr);
clearBtn.addEventListener('click', dataClear);


function checkSet() {
  
  for (let i = 0; i < input.length; i++){
    if(input[i].value == '' || isNaN(input[i].value) ) {
      alert('Enter valid number field: ' + (input[i].name) );
      arr = [];
      return 'err'
    } else {
      arr.push(input[i].value);
    }
  }
  console.log(arr);
  checkDoc();
  arr = [];
}

function checkDoc(){

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      p.innerHTML = this.responseText;
    }
  };
  xhttp.open("POST", "./php/check.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(`a=${arr[0]}&b=${arr[1]}&c=${arr[2]}`);
}

function switchDoc(){

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      h3.innerHTML = this.responseText;
    }
  };
  xhttp.open("POST", "./php/switch.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send();
}

function showArr(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      h3.innerHTML = this.responseText;
    }
  };
  xhttp.open("POST", "./php/array.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send();
}


function dataClear(){
  p.innerHTML = '';
  h3.innerHTML = '';
}